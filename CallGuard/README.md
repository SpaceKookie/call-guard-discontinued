# == Description ==
My Little Phony will check your calendars for events that have the availability flag set to BUSY and mute all of your incoming phone calls during that time. It also enables you to send back an auto-sms response to the caller telling them that you're busy.

Planned features:
- SMS Presets

- Calendar Selection (Including sync services)

- Getting the damn LED to work :)

- Further bug fixes and performance improvements
