package de.r2soft.callguard.cals;

import java.util.ArrayList;

import de.r2soft.callguard.types.Event;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;

/**
 * Will get event information from the built in calendars. Only called by the service when checking if a call should be muted
 * 
 * @author Katharina
 * 
 */
public class CalendarDealer {

	private ContentResolver cr;

	private static String EVENT_END = CalendarContract.Events.DTEND;
	private static String EVENT_START = CalendarContract.Events.DTSTART;
	private static String AVAILABILITY = CalendarContract.Events.AVAILABILITY;

	private static String[] INDICES = { EVENT_START, EVENT_END, AVAILABILITY };

	private static Uri uri = Events.CONTENT_URI;

	public CalendarDealer(Context context) {
		cr = context.getContentResolver();
	}

	public ArrayList<Event> getEvents(long interval) {

		String selection = EVENT_START + " > ? " + " AND " + EVENT_START + " < ?";
		String[] selectionArgs = { Long.toString(System.currentTimeMillis() - interval),
				Long.toString(System.currentTimeMillis() + interval) };

		Cursor c = cr.query(uri, INDICES, selection, selectionArgs, null);
		ArrayList<Event> events = new ArrayList<Event>();

		while (c.moveToNext())
			{

				long start = c.getLong(0);
				long end = c.getLong(1);
				int status = c.getInt(2);

				Event e = new Event(start, end, status);

				events.add(e);
			}
		c.close();
		return events;
	}

}
