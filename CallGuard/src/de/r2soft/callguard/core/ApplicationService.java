package de.r2soft.callguard.core;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import de.r2soft.callguard.utility.AppSettingsHelper;
import de.r2soft.callguard.utility.AppSettingsHelper.STATE;

public class ApplicationService extends IntentService {

	private boolean running;
	private STATE current;
	private STATE previous;

	public ApplicationService() {
		super(ApplicationService.class.getName());
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		running = true;
		previous = STATE.DEAD;

		while (running) {

			boolean app = AppSettingsHelper.getAppEnabledFlag(getBaseContext());
			boolean notification = AppSettingsHelper
					.getNotificationEnabledFlag(getBaseContext());
			boolean hide = AppSettingsHelper
					.getCenterEnabledFlag(getBaseContext());
			boolean led = AppSettingsHelper.getLEDEnabledFlag(getBaseContext());

			if (app && hide || !notification)
				current = STATE.HIDDEN;

			if (app && notification && !hide)
				current = STATE.ACTIVE;

			if (!app && notification)
				current = STATE.SLEEPING;

			if (!app && !notification)
				current = STATE.DEAD;

			if (previous != current) {
				if (!led)
					notifySomebodyElse(false);
				else
					notifySomebodyElse(true);
				previous = current;
			}

			if (current == STATE.HIDDEN) {
				stopForeground(true);
				return;
			}

			if (current == STATE.DEAD) {
				running = false;
				stopSelf();

			}
		}

	}

	private void notifySomebodyElse(boolean led) {

		String title = getString(R.string.notification_title_active);
		String msg = getString(R.string.notification_msg_active);
		Integer icon = R.drawable.ic_launcher;
		Intent intent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(
				getApplication(), 0, intent, Intent.FLAG_ACTIVITY_CLEAR_TASK);

		if (!led) {

			Notification.Builder not = new Notification.Builder(
					getBaseContext());
			not.setSmallIcon(icon).setContentTitle(title).setContentText(msg)
					.setContentIntent(pendingIntent);
			startForeground(Notification.FLAG_FOREGROUND_SERVICE, not.build());
		}

		else {

			Notification.Builder not = new Notification.Builder(
					getBaseContext());
			not.setPriority(Notification.PRIORITY_MAX).setSmallIcon(icon)
					.setContentTitle(title).setContentText(msg)
					.setContentIntent(pendingIntent)
					.setLights(0xff1493, 1000, 3000);
			startForeground(Notification.FLAG_FOREGROUND_SERVICE, not.build());
		}
	}

}
