package de.r2soft.callguard.core;

import java.util.ArrayList;
import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.provider.CalendarContract.Events;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import de.r2soft.callguard.cals.CalendarDealer;
import de.r2soft.callguard.db.DatabaseManager;
import de.r2soft.callguard.types.Call;
import de.r2soft.callguard.types.Event;
import de.r2soft.callguard.utility.AppSettingsHelper;

/**
 * BroadcastReceiver to register incoming phonecalls. Will be created and terminated with the ApplicationService.
 * 
 * @author Katharina
 * 
 */
public class CallReceiver extends BroadcastReceiver {

	private TelephonyManager man;
	private Context context;
	private SmsManager msgMan;
	private CalendarDealer dealer;

	public CallReceiver() {

	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("call911", "Registering the receiver");
		this.context = context;
		man = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		man.listen(phoneState, PhoneStateListener.LISTEN_CALL_STATE);
	}

	public void onDestroy() {
		Log.i("call911", "No longer listening to the phone state");
		man.listen(null, PhoneStateListener.LISTEN_NONE);
	}

	PhoneStateListener phoneState = new PhoneStateListener() {

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {

			/** Making sure that the current state is ringing */
			if (state == TelephonyManager.CALL_STATE_RINGING)
				{

					String mode = "Rang";

					if (AppSettingsHelper.getAppEnabledFlag(context))
						{
							if (AppSettingsHelper.getCallBlockMode(context) == 1)
								{
									AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
									audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);

									mode = "Muted";

									if (AppSettingsHelper.getSMSEnabledFlag(context))
										{

											if (incomingNumber != "Private")
												{
													msgMan.sendTextMessage(incomingNumber, null, context.getString(R.string.currently_busy), null, null);
													Toast.makeText(context, "Can't message private numbers :(", Toast.LENGTH_SHORT).show();
												}
										}
								}

							/** Will be called if user only wants meeting calls to be blocked */
							if (AppSettingsHelper.getCallBlockMode(context) == 2)
								{

									int statusCheck;
									dealer = new CalendarDealer(context);
									ArrayList<Event> events = dealer.getEvents(System.currentTimeMillis());

									for (int n = 0; n < events.size(); n++)
										{
											statusCheck = events.get(n).getStatus();

											if (statusCheck == Events.AVAILABILITY_BUSY)
												{
													AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
													audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);
													break;

												}

										}

								} else
								{
									Toast.makeText(context, "Pick a Call-Block-Mode next time", Toast.LENGTH_LONG).show();
								}

							@SuppressWarnings("deprecation")
							String date = new Date(java.lang.System.currentTimeMillis()).toLocaleString();

							Call call = new Call(numberCheck(incomingNumber), date, mode);

							DatabaseManager.getInstance().getDatabase().addCallInfo(call);

							Intent intent = new Intent();
							intent.setAction("MyLittlePhony.Trigger.Rebuild");
							intent.putExtra("intentTag", "rebuild");
							Log.i("data", "Sending Intent");
							context.sendBroadcast(intent);

						}
				}
		}
	};

	/**
	 * Called to check if there is a phone number known from the caller. Trying to message a "null" number will result in a crashing
	 * application
	 * 
	 * @param incomingNumber
	 * @return
	 */
	private String numberCheck(String incomingNumber) {
		return incomingNumber == null ? "Private" : incomingNumber;
	}

}
