package de.r2soft.callguard.core;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import de.r2soft.callguard.db.CallDatabase;
import de.r2soft.callguard.db.DatabaseManager;
import de.r2soft.callguard.fragments.OverviewFragment;
import de.r2soft.callguard.types.Call;
import de.r2soft.callguard.utility.AppSettingsHelper;
import de.r2soft.callguard.utility.ServiceStarter;

public class MainActivity extends Activity {

	// Declaring global variables everything nice and private
	private FragmentManager man;
	private FragmentTransaction trans;
	private Fragment overView;

	@Override
	protected void onCreate(Bundle states) {

		super.onCreate(states);
		setContentView(R.layout.activity_main);

		/** Creating singleton instance of the database */
		DatabaseManager j = DatabaseManager.getInstance();
		CallDatabase evil = new CallDatabase(getApplicationContext());
		j.setEvil_table(evil);

		makeReceiver();

		overView = new OverviewFragment();

		// Fragment Transaction to show PhoneFragment
		man = getFragmentManager();
		trans = man.beginTransaction();
		trans.replace(R.id.fragment_holder, overView, "OVERVIEW");
		trans.commit();
	}

	BroadcastReceiver receiver;

	private void makeReceiver() {

		if (AppSettingsHelper.getAppEnabledFlag(this))
			{
				new ServiceStarter(this);
				ServiceStarter.makeService();

				if (receiver == null)
					{
						receiver = new CallReceiver();
						registerReceiver(receiver, new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED));
					}

			} else if (!AppSettingsHelper.getAppEnabledFlag(this))
			{
				if (receiver != null)
					{
						unregisterReceiver(receiver);
						receiver = null;
					}
			}
	}

	@Override
	public void onResume() {
		super.onResume();
		makeReceiver();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	/** @return ArrayList<Call> being used in the CallAdapter. */
	public ArrayList<Call> getCallInformationArrayList() {
		return DatabaseManager.getInstance().getDatabase().getCallInfo();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	/** Starts the PreferencesFragment, built on the google Preferences API */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
		return true;
	}

}
