package de.r2soft.callguard.core;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import de.r2soft.callguard.fragments.SettingsFragment;

/**
 * Created SettingsFragment
 * 
 * @author Katharina
 * 
 */
public class SettingsActivity extends Activity {

	FragmentManager man;
	FragmentTransaction trans;

	@Override
	protected void onCreate(Bundle states) {

		super.onCreate(states);

		man = getFragmentManager();
		trans = man.beginTransaction();
		trans.replace(android.R.id.content, new SettingsFragment());
		trans.commit();
	}

	/**
	 * Calls the Phone Activity again to force the fragment to rebuild itself
	 */
	@Override
	public void onBackPressed() {

		super.onBackPressed();

	}
}