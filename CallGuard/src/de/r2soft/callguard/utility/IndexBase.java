package de.r2soft.callguard.utility;

/**
 * 
 * This class holds static Strings that represent keys used in my Preference Database
 * 
 * @author Katharina
 * 
 */
public class IndexBase {

	// Listener keys for the PreferenceOnChangeListener thingy thing
	public static final String KEY_ENABLE = "pref_enabled";
	public static final String KEY_SMS = "pref_sms";
	public static final String KEY_NOT = "pref_notification";
	public static final String KEY_LED = "pref_led";
	public static final String KEY_CEN = "pref_center";

	// My preferences name
	public static final String PHONY_APPLICATION_SETTINGS = "phony_prefs";
	public static final String KEY_CALL_MODE = "pref_mode";
}
