package de.r2soft.callguard.utility;

import de.r2soft.callguard.core.ApplicationService;
import android.content.Context;
import android.content.Intent;

/**
 * To re-create the service from anywhere in the app.
 * 
 * @author Katharina
 * 
 */
public class ServiceStarter {

	private static Context context;

	public ServiceStarter(Context context) {
		ServiceStarter.context = context;
	}

	public static void makeService() {
		Intent i = new Intent(context, ApplicationService.class);
		context.startService(i);

	}

}
