package de.r2soft.callguard.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Holds static methods to reference every possible Preference the app has access to. Is mostly being set by the SettingsFragment
 * and PhoneFragment.
 * 
 * @author Katharina
 * 
 */
public class AppSettingsHelper {

	/** The current state of the application */
	public static enum STATE {
		/** Blocking calls and showing a notification for that */
		ACTIVE,
		/** Blocking calls but not showing a notification for the service */
		HIDDEN,
		/** Showing the notification but not blocking calls */
		SLEEPING,
		/** Both the application and service have been shut down */
		DEAD;
	}

	private static SharedPreferences prefs;

	/**
	 * Application status
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getAppEnabledFlag(Context context) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		return prefs.getBoolean(IndexBase.KEY_ENABLE, true);
	}

	public static void setAppEnabledFlag(Context context, boolean newValue) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean(IndexBase.KEY_ENABLE, newValue);
		prefsEditor.commit();
	}

	/**
	 * Auto-SMS response status
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getSMSEnabledFlag(Context context) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		return prefs.getBoolean(IndexBase.KEY_SMS, false);
	}

	public static void setSMSEnabledFlag(Context context, boolean newValue) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean(IndexBase.KEY_SMS, newValue);
		prefsEditor.commit();
	}

	/**
	 * Notification submenu status
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getNotificationEnabledFlag(Context context) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		return prefs.getBoolean(IndexBase.KEY_NOT, true);
	}

	public static void setNotificationEnabledFlag(Context context, boolean newValue) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean(IndexBase.KEY_NOT, newValue);
		prefsEditor.commit();
	}

	/**
	 * LED Notification status
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getLEDEnabledFlag(Context context) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		return prefs.getBoolean(IndexBase.KEY_LED, false);
	}

	public static void setLEDEnabledFlag(Context context, boolean newValue) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean(IndexBase.KEY_LED, newValue);
		prefsEditor.commit();
	}

	/**
	 * Hide notification center notification
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getCenterEnabledFlag(Context context) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		return prefs.getBoolean(IndexBase.KEY_CEN, false);
	}

	public static void setCenterEnabledFlag(Context context, boolean newValue) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean(IndexBase.KEY_CEN, newValue);
		prefsEditor.commit();
	}

	/**
	 * 
	 * ADDITIONAL SETTINGS FOR THE MAIN APP ACTIVITY THAT WILL BE STORED IN SHAREDPREFERENCES AS WELL
	 * 
	 */

	public static int getCallBlockMode(Context context) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		return prefs.getInt(IndexBase.KEY_CALL_MODE, 0);
	}

	public static void setCallBlockMode(Context context, int newValue) {

		prefs = context.getSharedPreferences(IndexBase.PHONY_APPLICATION_SETTINGS, 0);
		Editor prefsEditor = prefs.edit();
		// 1 = block all, 2 = block from Cal, 0 = Reset to first time launch state
		if (newValue == 1 || newValue == 2 || newValue == 0)
			{
				prefsEditor.putInt(IndexBase.KEY_CALL_MODE, newValue);
				prefsEditor.commit();
			}
	}
}
