package de.r2soft.callguard.utility;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import de.r2soft.callguard.core.R;
import de.r2soft.callguard.db.DatabaseManager;
import de.r2soft.callguard.types.Call;

/**
 * Populates the ListView on OverView screen.
 * 
 * @author Katharina
 * 
 */
public class CallAdapter extends BaseAdapter {

	private ArrayList<Call> calls = new ArrayList<Call>();

	@Override
	public View getView(int position, View v, ViewGroup parent) {

		View row = v;
		LogHolder holder = null;

		// Create view if null
		if (row == null)
			{
				Context context = parent.getContext();
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.listview_item, parent, false);

				holder = new LogHolder();
				holder.position = position;

				holder.number = (TextView) row.findViewById(R.id.row_item_number);
				holder.time = (TextView) row.findViewById(R.id.row_item_date);
				holder.action = (TextView) row.findViewById(R.id.row_item_action);
				holder.respond = (Button) row.findViewById(R.id.abcdefg);
				row.setTag(holder);

			} else
			{
				holder = (LogHolder) row.getTag();
			}

		final ArrayList<Call> calls = DatabaseManager.getInstance().getDatabase().getCallInfo();

		row.setOnClickListener(new OnClickListener() {

			/**
			 * Get parent view or add button tag
			 */
			@Override
			public void onClick(View v) {

				int position = ((LogHolder) v.getTag()).position;
				Call call = calls.get(position);

				Uri number = Uri.parse("tel:" + call.getNumber());
				Intent dial = new Intent(Intent.ACTION_DIAL);
				dial.setData(number);
				v.getContext().startActivity(dial);
			}
		});

		// Set the correct values
		holder.number.setText(calls.get(position).getNumber());
		holder.time.setText(calls.get(position).getDate());
		holder.action.setText(calls.get(position).getAction());

		return row;
	}

	/**
	 * @param calls
	 *         the calls to set
	 */
	public void setCalls(ArrayList<Call> calls) {

		this.calls = calls;
	}

	@Override
	public int getCount() {

		return calls.size();
	}

	@Override
	public Object getItem(int position) {

		return calls.get(position);
	}

	public static class LogHolder {

		public int position;
		public TextView number;
		public TextView time;
		public TextView action;
		public Button respond;
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

}
