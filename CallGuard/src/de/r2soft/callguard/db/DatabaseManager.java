package de.r2soft.callguard.db;

/**
 * Singleton class to manage the database output (needed to make it accessible in the service
 * 
 * @author Katharina
 * 
 */
public class DatabaseManager {

	private CallDatabase evil_table;

	private static DatabaseManager instance = null;

	public static DatabaseManager getInstance() {
		if (instance == null)
			{
				instance = new DatabaseManager();
			}
		return instance;
	}

	public CallDatabase getDatabase() {
		return evil_table;
	}

	public void setEvil_table(CallDatabase table_reminders) {
		this.evil_table = table_reminders;
	}

	private DatabaseManager() {
	}

}