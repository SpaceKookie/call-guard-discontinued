package de.r2soft.callguard.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import de.r2soft.callguard.types.Call;

/**
 * SQL database. My best friends :) Stores call information. Also has methods to add an item and burn the entire table to the
 * ground
 * 
 * @author Katharina
 * 
 */
public class CallDatabase extends SQLiteOpenHelper {

	private final static String DATABASE_NAME = "CALL_ME_MAYBE";
	private final static String TABLE_NAME = "MUTED_CALL_INFO_TABLE";
	private final static int VERSION = 5;

	private final static String ID = "_id";
	private final static String NUMBER = "number";
	private final static String DATE = "date";
	private final static String ACTION = "action";

	private final static String[] ALL_COLUMNS = { ID, NUMBER, DATE, ACTION };

	private Context context;

	public CallDatabase(Context context, String name, CursorFactory factory, int version) {

		super(context, name, factory, version);
	}

	public CallDatabase(Context context) {

		super(context, DATABASE_NAME, null, VERSION);

		this.context = context;

		Log.i("Database", "Constructor called");
	}

	/**
	 * Makes database appear out of thin air
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {

		Log.i("Database", "Database create: begun!");
		StringBuilder b = new StringBuilder();
		b.append("CREATE TABLE " + TABLE_NAME + "(");
		b.append(ID + " " + "INTEGER PRIMARY KEY AUTOINCREMENT, ");
		b.append(NUMBER + " " + "TEXT, ");
		b.append(DATE + " " + "TEXT, ");
		b.append(ACTION + " " + "TEXT");
		b.append(");");

		db.execSQL(b.toString());
		Log.i("Database", "Database created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(db);
		Log.i("Database", "Database updated");
	}

	/**
	 * Adds a new call info to the DB
	 * 
	 * @param call
	 */
	public void addCallInfo(Call call) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues v = new ContentValues();

		v.put(NUMBER, call.getNumber());
		v.put(DATE, call.getDate());
		v.put(ACTION, call.getAction());

		db.insert(TABLE_NAME, null, v);
		db.close();
	}

	/**
	 * This method returns an ArrayList with all Call info objects. It's also a testemony to what happens if you continue programming
	 * after two glasses of wine :)
	 * 
	 * @return
	 */
	public ArrayList<Call> getCallInfo() {

		// Wild west code: it was hard to write, it should be hard to read
		SQLiteDatabase f = this.getReadableDatabase();
		ArrayList<Call> result = new ArrayList<Call>();
		Cursor c = f.query(TABLE_NAME, ALL_COLUMNS, null, null, null, null, null);

		while (c.moveToNext())
			{

				String a = c.getString(1);
				String b = c.getString(2);
				String d = c.getString(3);

				Call e = new Call(a, b, d);
				result.add(e);
			}
		f.close();

		return result;

	}

	// Deletes every entry from the database. Only called by the button "kill_it_with_fire"
	public void burnThemAll() {
		SQLiteDatabase f = this.getWritableDatabase();

		Log.i("data", "Burning...");

		f.delete(TABLE_NAME, null, null);
		f.close();

		Intent intent = new Intent();
		intent.setAction("MyLittlePhony.Trigger.Rebuild");
		intent.putExtra("intentTag", "rebuild");
		Log.i("data", "Sending Intent");
		context.sendBroadcast(intent);

	}
}