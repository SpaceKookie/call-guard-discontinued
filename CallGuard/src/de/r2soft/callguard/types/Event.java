package de.r2soft.callguard.types;

/**
 * An @Event item that holds all relevant information about the event.
 * 
 * @author Katharina
 * 
 */
public class Event {

	private long start;
	private long end;
	private int status;

	public Event(long s, long e, int st) {
		this.start = s;
		this.end = e;
		this.status = st;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
