package de.r2soft.callguard.types;

/**
 * Holds all information about a call.
 * 
 * @author Katharina
 * 
 */
public class Call {

	private String number;
	private String date;
	private String action;

	public Call(String number, String date, String action) {

		this.number = number;
		this.date = date;
		this.action = action;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {

		return number;
	}

	/**
	 * @param number
	 *         the number to set
	 */
	public void setNumber(String number) {

		this.number = number;
	}

	/**
	 * @return the date
	 */
	public String getDate() {

		return date;
	}

	/**
	 * @param date
	 *         the date to set
	 */
	public void setDate(String date) {

		this.date = date;
	}

	/**
	 * @return the action
	 */
	public String getAction() {

		return action;
	}

	/**
	 * @param action
	 *         the action to set
	 */
	public void setAction(String action) {

		this.action = action;
	}

}
