package de.r2soft.callguard.fragments;

import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import de.r2soft.callguard.core.R;
import de.r2soft.callguard.utility.AppSettingsHelper;
import de.r2soft.callguard.utility.IndexBase;

/**
 * This fragment is responsible for setting the SharedPreferences the app will
 * work with later. Implementing OnSahredPreferencesChangeListener instead of
 * OnPreferencesChangeListener for coding and performance reasons. Also using
 * the Google Settings API added in Android 3.0 (?)
 * 
 * @author Katharina
 * 
 */
public class SettingsFragment extends PreferenceFragment implements
		OnSharedPreferenceChangeListener {

	private CheckBoxPreference enabledPref;
	private CheckBoxPreference smsPref;
	private CheckBoxPreference notificationPref;
	private CheckBoxPreference ledPref;
	private CheckBoxPreference centerPref;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		getPreferenceManager().setSharedPreferencesName(
				IndexBase.PHONY_APPLICATION_SETTINGS);

		addPreferencesFromResource(R.layout.settings_layout);

		enabledPref = (CheckBoxPreference) getPreferenceScreen()
				.findPreference(IndexBase.KEY_ENABLE);
		smsPref = (CheckBoxPreference) getPreferenceScreen().findPreference(
				IndexBase.KEY_SMS);
		notificationPref = (CheckBoxPreference) getPreferenceScreen()
				.findPreference(IndexBase.KEY_NOT);
		ledPref = (CheckBoxPreference) getPreferenceScreen().findPreference(
				IndexBase.KEY_LED);
		centerPref = (CheckBoxPreference) getPreferenceScreen().findPreference(
				IndexBase.KEY_CEN);

		enabledPref.setChecked(AppSettingsHelper
				.getAppEnabledFlag(getActivity()));
		smsPref.setChecked(AppSettingsHelper.getSMSEnabledFlag(getActivity()));
		notificationPref.setChecked(AppSettingsHelper
				.getNotificationEnabledFlag(getActivity()));
		ledPref.setChecked(AppSettingsHelper.getLEDEnabledFlag(getActivity()));
		centerPref.setChecked(AppSettingsHelper
				.getCenterEnabledFlag(getActivity()));

		if (!AppSettingsHelper.getAppEnabledFlag(getActivity())) {
			smsPref.setEnabled(false);
			notificationPref.setEnabled(false);
			ledPref.setEnabled(false);
			centerPref.setEnabled(false);
		} else if (AppSettingsHelper.getAppEnabledFlag(getActivity())) {
			smsPref.setEnabled(true);
			notificationPref.setEnabled(true);

			if (AppSettingsHelper.getNotificationEnabledFlag(getActivity())) {
				ledPref.setEnabled(true);
				centerPref.setEnabled(true);
			}

			else if (!AppSettingsHelper
					.getNotificationEnabledFlag(getActivity())) {
				ledPref.setEnabled(false);
				centerPref.setEnabled(false);
			}
		}

	}

	@Override
	public void onResume() {

		super.onResume();

		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);

	}

	@Override
	public void onPause() {

		super.onPause();

		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	/**
	 * The actual Preference listener. Using the static AppSettingsHelper
	 * methodes to set the needed booleans and values.
	 */
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {

		if (key.equals(IndexBase.KEY_ENABLE)) {

			AppSettingsHelper.setAppEnabledFlag(getActivity(),
					sharedPreferences.getBoolean(IndexBase.KEY_ENABLE, true));

			AppSettingsHelper.setCallBlockMode(getActivity(), 0);

			// If the app was disabled the callmode will be reset to 0

			if (!AppSettingsHelper.getAppEnabledFlag(getActivity())) {
				smsPref.setEnabled(false);
				notificationPref.setEnabled(false);
				ledPref.setEnabled(false);
				centerPref.setEnabled(false);
			} else if (AppSettingsHelper.getAppEnabledFlag(getActivity())) {
				smsPref.setEnabled(true);
				notificationPref.setEnabled(true);
				if (AppSettingsHelper.getNotificationEnabledFlag(getActivity())) {
					ledPref.setEnabled(true);
					centerPref.setEnabled(true);
				}
			}

		} else if (key.equals(IndexBase.KEY_SMS)) {

			/** This handles the sms response */
			AppSettingsHelper.setSMSEnabledFlag(getActivity(),
					sharedPreferences.getBoolean(IndexBase.KEY_SMS, true));
		} else if (key.equals(IndexBase.KEY_NOT)) {
			AppSettingsHelper.setNotificationEnabledFlag(getActivity(),
					sharedPreferences.getBoolean(IndexBase.KEY_NOT, true));

			if (AppSettingsHelper.getNotificationEnabledFlag(getActivity())) {
				ledPref.setEnabled(true);
				centerPref.setEnabled(true);
			} else if (!AppSettingsHelper
					.getNotificationEnabledFlag(getActivity())) {
				ledPref.setEnabled(false);
				centerPref.setEnabled(false);
			}

		} else if (key.equals(IndexBase.KEY_LED)) {
			AppSettingsHelper.setLEDEnabledFlag(getActivity(),
					sharedPreferences.getBoolean(IndexBase.KEY_LED, true));
		} else if (key.equals(IndexBase.KEY_CEN)) {
			AppSettingsHelper.setCenterEnabledFlag(getActivity(),
					sharedPreferences.getBoolean(IndexBase.KEY_CEN, false));
		}

	}
}