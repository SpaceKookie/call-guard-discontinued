package de.r2soft.callguard.fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import de.r2soft.callguard.core.ApplicationService;
import de.r2soft.callguard.core.R;
import de.r2soft.callguard.db.CallDatabase;
import de.r2soft.callguard.db.DatabaseManager;
import de.r2soft.callguard.utility.AppSettingsHelper;
import de.r2soft.callguard.utility.CallAdapter;

public class OverviewFragment extends Fragment {

	private View view;
	private RadioButton muteAll;
	private RadioButton muteCal;
	private TextView response;
	private Context context;
	private ImageView imageView;
	private ListView list;
	private Button clear;
	private Intent intent;

	private IntentFilter filter = new IntentFilter("MyLittlePhony.Trigger.Rebuild");

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {

		context = getActivity();
		view = inflater.inflate(R.layout.layout_overview, container, false);
		buildUI();

		clear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				CallDatabase db = DatabaseManager.getInstance().getDatabase();
				db.burnThemAll();

				buildUI();
			}
		});

		muteAll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AppSettingsHelper.setCallBlockMode(context, 1);
				buildUI();

			}
		});

		muteCal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AppSettingsHelper.setCallBlockMode(context, 2);
				buildUI();
			}
		});

		return view;
	}

	/**
	 * This method is being called every time the gui needs to build. This will happen in the onCreate, in the onResume and on
	 * several other occasions like Intents triggering a rebuild.
	 */
	public void buildUI() {

		CallAdapter adapter = new CallAdapter();
		adapter.setCalls(DatabaseManager.getInstance().getDatabase().getCallInfo());

		response = (TextView) view.findViewById(R.id.status_view);
		muteAll = (RadioButton) view.findViewById(R.id.mute_all);
		muteCal = (RadioButton) view.findViewById(R.id.mute_with_cal);
		imageView = (ImageView) view.findViewById(R.id.sad_droid);
		clear = (Button) view.findViewById(R.id.fire_button);

		if (!AppSettingsHelper.getAppEnabledFlag(context))
			{
				clear.setEnabled(false);
			} else
			{
				clear.setEnabled(true);
			}

		list = (ListView) view.findViewById(R.id.missed_calls_list);
		list.setAdapter(adapter);

		if (!AppSettingsHelper.getAppEnabledFlag(context))
			{
				list.setEnabled(false);
			} else
			{
				list.setEnabled(true);
			}

		// Calling some methods to make the whole thing more readable. IMHO
		textViewLogic();
		radioLogic();
		drawDroid();
		makeService();

	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			Bundle extras = intent.getExtras();

			Log.i("data", "Receiving rebuild intent");

			String intentTag = (String) extras.get("intentTag");

			if (intentTag == "rebuild")
				{
					buildUI();
				}

		}
	};

	@Override
	public void onResume() {

		super.onResume();
		buildUI();
		context.registerReceiver(receiver, filter);
	}

	@Override
	public void onPause() {
		context.unregisterReceiver(receiver);
		super.onPause();
	}

	private void makeService() {

		if (AppSettingsHelper.getAppEnabledFlag(context))
			{

				if (AppSettingsHelper.getCenterEnabledFlag(getActivity()))
					{
						Log.i("Debug", "Sending Intent");
						intent = new Intent(getActivity(), ApplicationService.class);
						getActivity().startService(intent);
					}
			}
	}

	/** Draws the Android logo over radio buttons */
	private void drawDroid() {

		if (!AppSettingsHelper.getAppEnabledFlag(context))
			{
				imageView.setVisibility(View.VISIBLE);
			} else
			{
				imageView.setVisibility(View.GONE);
			}
	}

	/**
	 * Handles the logic that only one radio button can be active at a time.
	 * 
	 */
	private void radioLogic() {

		// Will either enable or disable the radio buttons (greying them out and making them unclickable)
		if (AppSettingsHelper.getAppEnabledFlag(context))
			{
				muteCal.setEnabled(true);
				muteAll.setEnabled(true);
			} else
			{
				muteCal.setEnabled(false);
				muteAll.setEnabled(false);

			}
		// Will decide which radiobutton to select based on what
		if (AppSettingsHelper.getCallBlockMode(context) == 0)
			{
				muteCal.setChecked(false);
				muteAll.setChecked(false);
			} else if (AppSettingsHelper.getCallBlockMode(context) == 1)
			{
				muteCal.setChecked(false);
				muteAll.setChecked(true);
			} else if (AppSettingsHelper.getCallBlockMode(context) == 2)
			{
				muteAll.setChecked(false);
				muteCal.setChecked(true);
			}
	}

	/**
	 * Handles the logic that the TextView object changes Strings and status according to the Application state
	 */
	private void textViewLogic() {

		if (AppSettingsHelper.getAppEnabledFlag(context))
			{
				response.setEnabled(true);
				if (AppSettingsHelper.getCallBlockMode(context) == 0)
					{
						response.setText(R.string.pick_n_choose);
					} else
					{
						response.setText(R.string.yes_using_mlp);
					}
			} else
			{
				response.setText(R.string.not_using_mlp);
				response.setEnabled(false);
			}
	}

}
